#define MAX_PLAYERS 5

#include <a_samp>
#include <streamer>
#include <sscanf2>
#include <color>
#include <dc_cmd>

#pragma tabsize 0

main()
{
	print("\n----------------------------------");
	print("  Radar Script test\n");
	print("----------------------------------\n");
	return 1;
}

new PlayerText:CamaraTD[MAX_PLAYERS];
#define MAX_RADARS 50
#define RADAR_OBJECT_ID 18880
#define RADAR_OBJECT_OFFSET 2.5
#define RADAR_STRING "{"#cYELLOW"}����� �: %d\n{"#cWHITE"}������������ ��������: \n< {"#cRED"}%d{"#cWHITE"} > km\\h"
#define RADAR_STRING_2 "{"#cYELLOW"}����� �: %d\n{"#cWHITE"}������������ ��������: \n<< {"#cLIGHTRED"}%d{"#cWHITE"} >> km\\h"

enum radars_data{
	radar_sql_id,
	radar_object_id,
	radar_area,
	Text3D:radar_text,
	radar_max_speed,
	Float:radar_x,
	Float:radar_y,
	Float:radar_z,
	Float:radar_rz,

	radar_text_timer,
	radar_timer_count

}
new _radar[MAX_RADARS][radars_data];
new rds[144];

public OnGameModeInit()
{
	for(new i=0;i<MAX_RADARS;i++)
	{
		_radar[i][radar_sql_id] = -1;
		_radar[i][radar_text_timer] = -1;
	}
	return 1;
}

stock InitRadar(Float:x, Float:y, Float:z,Float:minx, Float:miny, Float:maxx, Float:maxy, Float:angle, r_speed)
{
	new r = GetFreeRadarID(),
		radar_string[164];
	if(r == -1) return printf("Init Radar Error: MAX_COUNT_RADARS");

	_radar[r][radar_object_id] = CreateDynamicObject(RADAR_OBJECT_ID, x,y,z - RADAR_OBJECT_OFFSET,0.0,0.0,angle);
	_radar[r][radar_area] = CreateDynamicRectangle(minx, miny, maxx, maxy, -1, 0);
	_radar[r][radar_max_speed] = r_speed;
	
	//new sql_radar[512];
	//format(sql_radar,512,"INSERT INTO ")
	//_radar[r][radar_sql_id] = mysql_insert_id();

	format(radar_string,sizeof(radar_string),RADAR_STRING, r + 1,_radar[r][radar_max_speed]);
	_radar[r][radar_text] = CreateDynamic3DTextLabel(radar_string,-1,x,y,z + (RADAR_OBJECT_OFFSET + 0.5),50.0);
	if(_radar[r][radar_text_timer] != -1) KillTimer(_radar[r][radar_text_timer]);
	SetTimerEx("UpdateLabelRadar", 400,1,"i", r);
	printf("����� #%d ������", r + 1);
	return true;
}

forward UpdateLabelRadar(r);
public UpdateLabelRadar(r)
{
	new radar_string[164];
	format(radar_string,sizeof(radar_string),(_radar[r][radar_timer_count]==1) ? RADAR_STRING : RADAR_STRING_2, r + 1,_radar[r][radar_max_speed]);
	UpdateDynamic3DTextLabelText(_radar[r][radar_text],-1,radar_string);
	_radar[r][radar_timer_count] = !(_radar[r][radar_timer_count]==1);
	return 1;
}

CMD:getpos(playerid)
{
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x,y,z);
	format(rds,144,"%.2f %.2f %.2f",x,y,z);
	SendClientMessage(playerid,-1,rds);
	return 1;
}

CMD:initradar(playerid, params[])
{
	new Float:x, Float:y, Float:z,
		Float:minx, Float:miny, Float:maxx, Float:maxy, Float:angle, radar_speed;
	GetPlayerPos(playerid,x,y,z);
	GetPlayerFacingAngle(playerid, angle);
	if(sscanf(params,"ffffi", minx,miny,maxx,maxy,radar_speed)) return SendClientMessage(playerid,-1,"/initradar [minx] [miny] [maxx] [maxy] [radar_speed]");
	InitRadar(Float:x, Float:y, Float:z, Float:minx, Float:miny, Float:maxx, Float:maxy, Float:angle, radar_speed);
	return true;
}

CMD:veh(playerid,params[])
{
	if(sscanf(params,"ddd",params[0],params[1],params[2])) return	SendClientMessage(playerid, CWHITE, " Use: /veh [carid] [????1] [????2]");
	if(params[0] < 400 || params[0] > 611) return SendClientMessage(playerid, CWHITE, "invalid model");
	if(params[1] < 0 || params[1] > 250) return SendClientMessage(playerid, CWHITE, "invalid color 1");
	if(params[2] < 0 || params[2] > 250) return SendClientMessage(playerid, CWHITE, "invalid color 2");
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X,Y,Z);
	new veh_id = 0 ;
	veh_id = CreateVehicle(params[0], X,Y,Z, 0.0, params[1], params[2], 600_000);
	if(!veh_id) return SendClientMessage(playerid,-1,"#0111");
	LinkVehicleToInterior(veh_id, GetPlayerInterior(playerid));
	SetVehicleVirtualWorld(veh_id,GetPlayerVirtualWorld(playerid));
	return 1;
}

stock GetNearestRadar(playerid)
{
	for(new i=0;i<MAX_RADARS;i++)
	{
			if(IsPlayerInDynamicArea(playerid, _radar[i][radar_area])) return i;
	}
	return -1;
}

public OnPlayerConnect(playerid) {
	CamaraTD[playerid] = CreatePlayerTextDraw(playerid,-200.000000, -8.000000, "_");
	PlayerTextDrawBackgroundColor(playerid,CamaraTD[playerid], 255);
	PlayerTextDrawFont(playerid,CamaraTD[playerid], 1);
	PlayerTextDrawLetterSize(playerid,CamaraTD[playerid], 0.500000, 64.000000);
	PlayerTextDrawColor(playerid,CamaraTD[playerid], -1);
	PlayerTextDrawSetOutline(playerid,CamaraTD[playerid], 0);
	PlayerTextDrawSetProportional(playerid,CamaraTD[playerid], 1);
	PlayerTextDrawSetShadow(playerid,CamaraTD[playerid], 1);
	PlayerTextDrawUseBox(playerid,CamaraTD[playerid], 1);
	PlayerTextDrawBoxColor(playerid,CamaraTD[playerid], 0xFFFFFF55);
	PlayerTextDrawTextSize(playerid,CamaraTD[playerid], 800.000000, 212.000000);
	return true;
}
public OnPlayerDisconnect(playerid, reason) {
    PlayerTextDrawDestroy(playerid,CamaraTD[playerid]);
    
	return true;
}
public OnPlayerDeath(playerid, killerid, reason) {
    PlayerTextDrawHide(playerid, CamaraTD[playerid]);
    
	return true;
}

public OnPlayerUpdate(playerid)
{
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
	{
		new r = GetNearestRadar(playerid),
		radar_string[144];
		if(r != -1 && GetPVarInt(playerid, "v_radar") == 0)
		{
				new Float:STt[4], v_id = GetPlayerVehicleID(playerid);
				GetVehicleVelocity(v_id,STt[0],STt[1],STt[2]);
				STt[3] = floatsqroot(floatpower(floatabs(STt[0]), 2.0) + floatpower(floatabs(STt[1]), 2.0) + floatpower(floatabs(STt[2]), 2.0)) * 100.3;
				if(floatround(STt[3]) > _radar[r][radar_max_speed])
				{
						format(radar_string, 144, "~g~YOUR SPEED: ~w~%d ~g~ km\\h ~n~~r~MAX SPEED: ~w~%d ~r~ km\\h ~n~~n~DRIVE OFF!", floatround(STt[3]),_radar[r][radar_max_speed]);
						GameTextForPlayer(playerid, radar_string, 10000, 4);
						SetVehicleVelocity(v_id, STt[0] / 4,STt[1] / 4, STt[2] + 0.02);
						SetPVarInt(playerid, "v_radar", true);
						FlashPlayer(playerid);
				}
		}
		else if(GetPVarInt(playerid, "v_radar") != 0) DeletePVar(playerid, "v_radar");
	}
	return 1;
}

stock GetFreeRadarID(){
	for(new i=0;i<MAX_RADARS;i++)
	{
		if(_radar[i][radar_sql_id] == -1) return i;
	}
	return -1;
}
stock FlashPlayer(playerid)
{
    if(!IsPlayerConnected(playerid)) return true;
    PlayerTextDrawShow(playerid, CamaraTD[playerid]);
	SetTimerEx("DataEfectoCamara", 500, false, "d", playerid);
 	return true;
}
forward DataEfectoCamara(playerid);
public DataEfectoCamara(playerid) {
    if(IsPlayerConnected(playerid)) PlayerTextDrawHide(playerid, CamaraTD[playerid]);
    return 1;
}