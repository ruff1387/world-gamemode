forward AC_OnPlayerConnect(playerid);
#if defined OnPlayerConnect
	#undef OnPlayerConnect
#endif
#define OnPlayerConnect AC_OnPlayerConnect

forward AC_OnGameModeInit();
#if defined OnGameModeInit
	#undef OnGameModeInit
#endif
#define OnGameModeInit AC_OnGameModeInit