
new	bool: CheckGun[MAX_PLAYERS];
new	ac__GUN[MAX_PLAYERS][13];
new	ac__AMMO[MAX_PLAYERS][13];
new	ac__WEAPON[MAX_PLAYERS][13];
new	ac__AMMO_CHECK[MAX_PLAYERS][13];
new Check_Gun[MAX_PLAYERS char];

public OnPlayerConnect(playerid)
{
	Check_Gun{playerid} = 0;
	#if defined ac__OnPlayerConn  
        ac__OnPlayerConn(playerid);
    #endif
	return 1;
}
#if    defined    _ALS_OnPlayerConn    
    #undef    OnPlayerConn
#else
    #define    _ALS_OnPlayerConn
#endif
#define    OnPlayerConn    ac__OnPlayerConn
#if    defined    ac__OnPlayerConn
forward  ac__OnPlayerConn(playerid);
#endif  


public OnPlayerUpdate(playerid)
{
	for (new slot = 1; slot != 13; slot++)
	{
		GetPlayerWeaponData(playerid, slot, ac__GUN[playerid][slot], ac__AMMO_CHECK[playerid][slot]);
		if (!(ac__WEAPON[playerid][slot] == ac__GUN[playerid][slot]))
		{
			CheckGun[playerid] = true;
			break;
		}
		if (GetPlayerWeapon(playerid) == ac__WEAPON[playerid][0]) 
		{
			if(ac__AMMO_CHECK[playerid][0] != 0) return 1;
			SetTimerEx("dgun",1000,0,"iii",playerid,GetPlayerWeapon(playerid),slot);
			return 1;
		}
		if (ac__AMMO_CHECK[playerid][slot] > ac__AMMO[playerid][slot])
		{
			CheckGun[playerid] = true;
			break;
		} 
		else 
		{
			ac__AMMO[playerid][slot] = ac__AMMO_CHECK[playerid][slot];
			return 1;
		}
	}
	if (CheckGun[playerid])
	{
		if(Check_Gun{playerid} > 2 && Check_Gun{playerid} != 3) 
		{
			SendClientMessage(playerid, 0xFF8BABFF, "[�������]: {EAEAEA}��������� ��� �� {ff8bab}�������{EAEAEA}.");
			CheckGun[playerid] = true;
			return 1;
		}
		else if(Check_Gun{playerid} <= 2)
		{
			ResetPlayerWeapons(playerid);
			Check_Gun{playerid}++;
			return 1;
		}
		else
		{
			Kick(playerid);
		}
		CheckGun[playerid] = false;
	}
	#if defined ac__OnPlayerUpd
        ac__OnPlayerUpd(playerid);
    #endif
	return 1;
}
#if    defined    _ALS_OnPlayerUpd   
    #undef    OnPlayerUpd
#else
    #define    _ALS_OnPlayerUpd
#endif
#define    OnPlayerUpd    ac__OnPlayerUpd
#if    defined    ac__OnPlayerUpd
forward  ac__OnPlayerUpd(playerid);
#endif  


forward dgun(playerid,gun,slot);
public dgun(playerid,gun,slot)
{
	new i = playerid;
	GetPlayerWeaponData(i, slot, ac__WEAPON[i][slot], ac__AMMO_CHECK[i][slot]);
	if ((GetPlayerWeapon(i) == gun) && (ac__AMMO_CHECK[i][slot] == 0))
	{
		CheckGun[i] = true;
	}
	return 1;
}
 
stock ac__GivePlayerWeapon(i,w,a)
{
	ac__GUN[i][ac__GetWeaponSlot(w)] = w;
	ac__AMMO[i][ac__GetWeaponSlot(w)] = a;
	return GivePlayerWeapon(i,w,a);
}
#if    defined    _ALS_GivePlayerWeapon
    #undef    GivePlayerWeapon
#else
    #define    _ALS_GivePlayerWeapon
#endif
#define GivePlayerWeapon ac__GivePlayerWeapon 
 
stock ac__ResetPlayerWeapons(i)
{
	for (new x = 1; x != 13; x++)
	{
		ac__GUN[i][x] = 0;
		ac__AMMO[i][x] = 0;
	}
	return ResetPlayerWeapons(i);
}
#if    defined    _ALS_ResetPlayerWeapons
    #undef    ResetPlayerWeapons
#else
    #define    _ALS_ResetPlayerWeapons
#endif
#define ResetPlayerWeapons ac__ResetPlayerWeapons

 
stock ac__GetWeaponSlot(weaponid)
{
	new slot;
	switch(weaponid)
	{
		case 0,1: slot = 0;
		case 2 .. 9: slot = 1;
		case 10 .. 15: slot = 10;
		case 16 .. 18, 39: slot = 8;
		case 22 .. 24: slot =2;
		case 25 .. 27: slot = 3;
		case 28, 29, 32: slot = 4;
		case 30, 31: slot = 5;
		case 33, 34: slot = 6;
		case 35 .. 38: slot = 7;
		case 40: slot = 12;
		case 41 .. 43: slot = 9;
		case 44 .. 46: slot = 11;
	}
	return slot;
}