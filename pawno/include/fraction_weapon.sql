-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 22 2016 г., 20:33
-- Версия сервера: 5.1.73-log
-- Версия PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `testwjmo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `fraction_weapon`
--

CREATE TABLE IF NOT EXISTS `fraction_weapon` (
  `fwID` int(11) NOT NULL AUTO_INCREMENT,
  `Slot` int(11) NOT NULL,
  `Fraction` int(11) NOT NULL,
  `fwName` varchar(32) NOT NULL,
  `fwGunID` int(11) NOT NULL DEFAULT '0',
  `fwGunAmmo` int(11) NOT NULL DEFAULT '0',
  `fwArmor` int(11) NOT NULL DEFAULT '0',
  `fwRank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fwID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
