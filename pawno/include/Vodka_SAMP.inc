/*
    ������  ��� ������� 'Vodka_SAMP' v2.0!
    ������, ���������� ��������� ��� � ������ ����.
		1)����������� ������� ���� � ����� 'pawno/include'
		2)����������� �� ���� ����� �������� � ����� ����� '#include <a_samp>'
			��������� '#include <Vodka_SAMP>'
    ���������: [KrYpToDeN]
    �����: kryptoden
*/
#include <a_samp>
#if !defined _string_included
	#error Include 'Vodka_SAMP.inc' should be loaded after 'string.inc'!
#endif

#if defined _VodkaSAMP_included
	#endinput
#endif

#define _VodkaSAMP_included
#pragma library VodkaSAMP

native KryptoHook_GetPlayerName(name[]);

stock _GetPlayerName(playerid, name[], length)
{
    GetPlayerName(playerid, name, length);
    KryptoHook_GetPlayerName(name);
}
#if defined _ALS_GetPlayerName
    #undef GetPlayerName
#else
    #define _ALS_GetPlayerName
#endif
#define GetPlayerName _GetPlayerName

/*
    ������  ��� ������� 'Vodka_SAMP' v2.0!
    ������, ���������� ��������� ��� � ������ ����.
		1)����������� ������� ���� � ����� 'pawno/include'
		2)����������� �� ���� ����� �������� � ����� ����� '#include <a_samp>'
			��������� '#include <Vodka_SAMP>'
    ���������: [KrYpToDeN]
    �����: kryptoden
*/