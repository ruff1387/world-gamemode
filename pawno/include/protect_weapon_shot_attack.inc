#define             MAX_AUTOBULLET_INFRACTIONS          3
#define             AUTOBULLET_RESET_DELAY              30

static AutoBulletInfractions[MAX_PLAYERS];
static LastInfractionTime[MAX_PLAYERS];

forward OnAntiCheatAutoBullet(playerid, weaponid);

public OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ)
{
    if(!IsPlayerInAnyVehicle(playerid))
    {
        switch(weaponid)
        {
            case 27, 23, 25, 29, 30, 31, 33, 24, 38:
            {
                if(CheckSpeed(playerid))
                {
                    if(gettime() - LastInfractionTime[playerid] >= AUTOBULLET_RESET_DELAY) AutoBulletInfractions[playerid] = 1;
                    else AutoBulletInfractions[playerid]++;
                    LastInfractionTime[playerid] = gettime();

                    if(AutoBulletInfractions[playerid] == MAX_AUTOBULLET_INFRACTIONS)
                    {
                        AutoBulletInfractions[playerid] = 0;
                        CallLocalFunction("OnAntiCheatAutoBullet", "ii", playerid, weaponid);
                        return 0;
                    }
                }
            }
        }
    }
    
    if (funcidx("ACAutoB_OnPlayerWeaponShot") != -1)
    {
        return CallLocalFunction("ACAutoB_OnPlayerWeaponShot", "iiiifff", playerid, weaponid, hittype, hitid, fX, fY, fZ);
    }
    return 1;
}

#if defined _ALS_OnPlayerWeaponShot
    #undef OnPlayerWeaponShot
#else
    #define _ALS_OnPlayerWeaponShot
#endif
#define OnPlayerWeaponShot ACAutoB_OnPlayerWeaponShot

forward ACAutoB_OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ);

public OnPlayerDisconnect(playerid, reason)
{
    AutoBulletInfractions[playerid] = 0;

    if (funcidx("ACAutoB_OnPlayerDisconnect") != -1)
    {
        return CallLocalFunction("ACAutoB_OnPlayerDisconnect", "ii", playerid, reason);
    }
    return 1;
}

#if defined _ALS_OnPlayerDisconnect
    #undef OnPlayerDisconnect
#else
    #define _ALS_OnPlayerDisconnect
#endif
#define OnPlayerDisconnect ACAutoB_OnPlayerDisconnect

forward ACAutoB_OnPlayerDisconnect(playerid, reason);


static CheckSpeed(playerid)
{
    new Keys,ud,lr;
    GetPlayerKeys(playerid,Keys,ud,lr);

    if(ud == KEY_UP && lr != KEY_LEFT && lr != KEY_RIGHT)
    {
        new Float:Velocity[3];
        GetPlayerVelocity(playerid, Velocity[0], Velocity[1], Velocity[2]);
        Velocity[0] = floatsqroot( (Velocity[0]*Velocity[0])+(Velocity[1]*Velocity[1])+(Velocity[2]*Velocity[2]));
        if(Velocity[0] >= 0.11 && Velocity[0] <= 0.13) return 1;
    }
    return 0;
}