-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 30 2016 г., 08:25
-- Версия сервера: 5.1.73-log
-- Версия PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `testwjmo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `perenos_player`
--

CREATE TABLE IF NOT EXISTS `perenos_player` (
  `name` varchar(24) NOT NULL,
  `accountid` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `paydayhad` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `zakonp` int(11) NOT NULL,
  `member` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `job` int(11) NOT NULL,
  `metall` int(11) NOT NULL,
  `patron` int(11) NOT NULL,
  `drugs` int(11) NOT NULL,
  `house` int(11) NOT NULL,
  `flat` int(11) NOT NULL,
  `biz` int(11) NOT NULL,
  `car1` int(11) NOT NULL,
  `car2` int(11) NOT NULL,
  `donaterank` int(11) NOT NULL,
  `donatemoney` int(11) NOT NULL,
  `activate` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
