static 
		Break[MAX_PLAYERS],
		Float: BreakPosition [MAX_PLAYERS] [6],
		Spawned[MAX_PLAYERS],
		ac__MAX_PLAYERS = 0,
		LagServer[MAX_PLAYERS]
;
	
public OnPlayerDisconnect(playerid, reason)	{
	Break[playerid] = 0;
	ac__MAX_PLAYERS--;
	#if defined ac__OnPlayerDisconnect
        ac__OnPlayerDisconnect(playerid, reason);
    #endif
	return 1;
}
public OnPlayerDeath(playerid,killerid,reason)	{
	Break[playerid] = 3;
	#if defined ac__OnPlayerDeath
        ac__OnPlayerDeath(playerid,killerid,reason);
    #endif
	return 1;
}
public OnPlayerSpawn(playerid)	{
	Break[playerid] = 4;
	Spawned[playerid] = 1;
	#if defined ac__OnPlayerSpawn
        ac__OnPlayerSpawn(playerid);
    #endif
	return 1;
}
public OnPlayerConnect(playerid)	{
	Break[playerid] = -1;
	Spawned[playerid] = -1;
	if(ac__MAX_PLAYERS <= MAX_PLAYERS) ac__MAX_PLAYERS++;
	#if defined ac__OnPlayerConnect
        ac__OnPlayerConnect(playerid);
    #endif
	return 1;
}

public OnGameModeInit()
{
	SetTimer("ac__Check", 1000, true);
 	SetTimer("ac__Check_Three", 3*1000, true);
	#if defined ac__OnGameModeInit
        ac__OnGameModeInit();
    #endif
	return 1;
}

forward ac__Check_Three();
public ac__Check_Three()
{
	for(new i = 0; i != ac__MAX_PLAYERS; i++)
	{
		if(Spawned[i] == 1)
		{
			if(GetPlayerState(i) == 2)
			{
				GetPlayerPos(i, BreakPosition[i][3], BreakPosition[i][4], BreakPosition[i][5]);
				if(BreakPosition[i][5] > 550.0)
				{
					BreakPosition[i][0] = 0.0;
					BreakPosition[i][1] = 0.0;
				}
			}
			if(BreakPosition[i][5] < 550.0 && BreakPosition[i][3] != 0.0)
			{
				BreakPosition[i][0] = BreakPosition[i][3];
				BreakPosition[i][1] = BreakPosition[i][4];
			}
		}
	}
	return 1;
}

forward ac__Check();
public ac__Check()
{
	for(new playerid = 0; playerid != ac__MAX_PLAYERS; playerid++)
	{
		if(GetPVarInt(playerid,"OnPlayerCheat") == 1) 
		{
			Kick(playerid);
			return 1;
		}
		if(Break[playerid] > 0) Break[playerid]--;
		GetPlayerPos(playerid, BreakPosition[playerid][3], BreakPosition[playerid][4], BreakPosition[playerid][5]);
		if(BreakPosition[playerid][5] > 550.0)
		{
			BreakPosition[playerid][0] = 0.0;
			BreakPosition[playerid][1] = 0.0;
		}
		if(GetTickCount() - LagServer[playerid] > 2000)
		{
			BreakPosition[playerid][0] = 0.0;
			BreakPosition[playerid][1] = 0.0;
		}
		if(GetPlayerSurfingVehicleID(playerid) != INVALID_VEHICLE_ID)
		{
			BreakPosition[playerid][0] = 0.0;
			BreakPosition[playerid][1] = 0.0;
		}
		if(BreakPosition[playerid][0] != 0.0)
		{
			new Float:xdist = BreakPosition[playerid][3]-BreakPosition[playerid][0];
			new Float:ydist = BreakPosition[playerid][4]-BreakPosition[playerid][1];
			new Float:sqxdist = xdist*xdist;
			new Float:sqydist = ydist*ydist;
			new Float:distance = (sqxdist+sqydist)/31;
			if(distance < 800 && distance > 140 && Break[playerid] == 0 && GetPlayerState(playerid) == PLAYER_STATE_DRIVER && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
			{
				CallLocalFunction("OnPlayerCheat", "iis", playerid,669966,"Car Air Break");
				return 1;
			}
			if(distance < 800 && distance > 80 && Break[playerid] == 0 && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
			{
				CallLocalFunction("OnPlayerCheat", "iis", playerid,669966,"Air Break");
				return 1;
			}
			else
			{
				if(distance < 800 && distance > 82 && Break[playerid] == 0 && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
				{
					CallLocalFunction("OnPlayerCheat", "iis", playerid,669966,"Air Break");
					return 1;
				}
			}
			if(distance > 800 && Break[playerid] == 0 && GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
			{
				CallLocalFunction("OnPlayerCheat", "iis", playerid,669966,"Teleport");
				return 1;
			}
			if(distance > 800 && Break[playerid] == 0 && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
			{
				CallLocalFunction("OnPlayerCheat", "iis", playerid,669966,"Teleport");
				return 1;
			}
		}
		if(BreakPosition[playerid][5] < 550.0 && BreakPosition[playerid][3] != 0.0)
		{
			BreakPosition[playerid][0] = BreakPosition[playerid][3];
			BreakPosition[playerid][1] = BreakPosition[playerid][4];
		}
		LagServer[playerid] = GetTickCount();
	}
	return 1;
}

stock ac__SetVehiclePos(vehicleid, Float:X, Float:Y, Float:Z)
{
	for(new playerid = 0; playerid != ac__MAX_PLAYERS; playerid++)
	{
	    if(GetPlayerVehicleID(i) == vehicleid)
	    {
    		Break[i] = 5;
    		BreakPosition[i][0] = 0.0;
			BreakPosition[i][1] = 0.0;
		}
	}
	return SetVehiclePos(vehicleid ,X,Y,Z);
}


stock ac__SetPlayerPos(playerid,Float:X,Float:Y,Float:Z)
{
    Break[playerid] = 5;
	return SetPlayerPos(playerid,X,Y,Z);
}

#if    defined    _ALS_SetVehiclePos
    #undef    SetVehiclePos
#else
    #define    _ALS_SetVehiclePos
#endif
#define SetVehiclePos ac__SetVehiclePos

#if    defined    _ALS_SetPlayerPos
    #undef    SetPlayerPos
#else
    #define    _ALS_SetPlayerPos
#endif
#define SetPlayerPos ac__SetPlayerPos 

#if    defined    _ALS_OnGameModeInit
    #undef    OnGameModeInit
#else
    #define    _ALS_OnGameModeInit
#endif
#define    OnGameModeInit    ac__OnGameModeInit
#if    defined    ac__OnGameModeInit
forward    ac__OnGameModeInit();
#endif 

#if    defined    _ALS_OnPlayerDeath
    #undef    OnPlayerDeath
#else
    #define    _ALS_OnPlayerDeath
#endif
#define    OnPlayerDeath    ac__OnPlayerDeath
#if    defined    ac__OnPlayerDeath
forward    ac__OnPlayerDeath(playerid,killerid,reason);
#endif 

#if    defined    _ALS_OnPlayerSpawn
    #undef    OnPlayerSpawn
#else
    #define    _ALS_OnPlayerSpawn
#endif
#define    OnPlayerSpawn    ac__OnPlayerSpawn
#if    defined    ac__OnPlayerSpawn
forward    ac__OnPlayerSpawn(playerid);
#endif 

#if    defined    _ALS_OnPlayerConnect 
    #undef    OnPlayerConnect
#else
    #define    _ALS_OnPlayerConnect
#endif
#define    OnPlayerConnect    ac__OnPlayerConnect
#if    defined    ac__OnPlayerConnect
forward    ac__OnPlayerConnect(playerid);
#endif 

#if    defined    _ALS_OnPlayerDisconnect  
    #undef    OnPlayerDisconnect
#else
    #define    _ALS_OnPlayerDisconnect
#endif
#define    OnPlayerDisconnect    ac__OnPlayerDisconnect
#if    defined    ac__OnPlayerDisconnect
forward    ac__OnPlayerDisconnect(playerid, reason);
#endif 

forward OnPlayerCheat(playerid, code, reason[]);

