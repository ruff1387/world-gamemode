public OnPlayerDisconnect(playerid)
{
   new Float: SavePosition[3],
	query_string[128]; 
	
	GetPlayerPos(playerid, SavePosition[0], SavePosition[1], SavePosition[2]); 

	format(query_string, sizeof(query_string), 
   "UPDATE "TABLE_ACCOUNT" SET `"PosX"` = '%f', `"PosY"` = '%f', `"PosZ"` = '%f', \ 
   `"Int"` = '%d', `"World"` = '%d' WHERE `"Name"` = '%s'", 
	SavePosition[0], 
	SavePosition[1], 
	SavePosition[2],
	GetPlayerInterior(playerid),
	GetPlayerVirtualWorld(playerid)); 
	mysql_function_query(connectionHandle, query_string, 0, "","");
   #if defined t__OnPlayerDisconnect
	   return t__OnPlayerDisconnect(playerid);
   #endif
}
	

stock SetPositionExit(playerid) 
{  
    SetPlayerPos(playerid, 
	cache_get_field_content_float(0,""PosX"",connectionHandle),
	cache_get_field_content_float(0,""PosY"",connectionHandle),
	cache_get_field_content_float(0,""PosZ"",connectionHandle)); 
    SetPlayerVirtualWorld(playerid, cache_get_field_content_int(0,""World"",connectionHandle)); 
    SetPlayerInterior(playerid, cache_get_field_content_int(0,""Int"",connectionHandle)); 
    return 1; 
}  


#if defined _ALS_OnPlayerDisconnect
    #undef    OnPlayerDisconnect
#else
    #define    _ALS_OnPlayerDisconnect
#endif
#define    OnPlayerDisconnect    t__OnPlayerDisconnect
#if defined t__OnPlayerDisconnect
	forward t__OnPlayerDisconnect(playerid);
#endif  