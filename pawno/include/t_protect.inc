static jf_ip [MAX_PLAYERS] [64]; 
static jf_count [MAX_PLAYERS];
static _jf_count = 0;

public OnIncomingConnection(playerid, ip_address[], port)
{
	if (playerid >= MAX_PLAYERS)
	{
		new jf_ban [64];
		BlockIpAddress(ip_address,60*1000*5);
		format(jf_ban, sizeof(jf_ban), "banip %s", ip_address);
		SendRconCommand(jf_ban);		
	}
	_jf_count ++;
	if (_jf_count >= 10)
	{
		new jf_ban [64];
		BlockIpAddress(ip_address,60*1000*5);
		format(jf_ban, sizeof(jf_ban), "banip %s", ip_address);
		SendRconCommand(jf_ban);
		strmid (jf_ip [playerid], ip_address, 0, strlen (ip_address), 64);	
	}
	for (new i = 0; i != MAX_PLAYERS; i++)
	{
		if(!strcmp(jf_ip[i], ip_address,true))
		{
			jf_count[i]++;
			if (jf_count[i] > 2)
			{
				new jf_ban [64];
				BlockIpAddress(ip_address,60*1000*5);
				format(jf_ban, sizeof(jf_ban), "banip %s", ip_address);
				SendRconCommand(jf_ban);
				strmid (jf_ip[i],  "NONE", 0, strlen ("NONE"), 5);
			}
			break; 
		}
		else 
		{
			strmid (jf_ip[i],  ip_address, 0, strlen (ip_address), 32);
			break;
		}
	}
	#if defined bot__OnIncomingConnection
		bot__OnIncomingConnection(playerid, ip_address, port);
	#endif
	return 1;
}


public OnPlayerConnect(playerid)
{
	new bot__ip[24];
	GetPlayerIp (playerid, bot__ip, 24);
	for (new i = 0; i != MAX_PLAYERS; i++)
	{
		if(!strcmp(jf_ip[i], bot__ip, true))
		{
			if (jf_count[i] > 2)
			{
				SetPVarInt (playerid, "JF", 1);
				break;
			}
		}
	}
	#if defined bot__OnPlayerConnect
        bot__OnPlayerConnect(playerid);
    #endif
	return 1;
}

public OnGameModeInit()
{
	for (new i = 0; i != MAX_PLAYERS; i++)
	{
		strmid ( jf_ip[i],  "NONE", 0, strlen ("NONE"), 5);
		jf_count[i] = 0;
	}
	SetTimer("JF_TIMER", 5*1000, true);
	#if defined bot__OnGameModeInit
        	bot__OnGameModeInit();
    	#endif
	return 1;
}

forward JF_TIMER();
public JF_TIMER()
{
	_jf_count = 0;
	for (new i = 0; i != MAX_PLAYERS; i++)
	{
		if(strcmp(jf_ip[i], "NONE", true))
		{
			new jf_ban [64];
			format(jf_ban, sizeof(jf_ban), "unbanip %s", jf_ip[i]);
			SendRconCommand(jf_ban);
			break;
		}
		strmid (jf_ip[i],  "NONE", 0, strlen ("NONE"), 5);
	}
	return 1;
}

#if    defined    _ALS_OnPlayerConnect
    #undef    OnPlayerConnect
#else
    #define    _ALS_OnPlayerConnect
#endif
#define    OnPlayerConnect    bot__OnPlayerConnect
#if    defined    bot__OnPlayerConnect
forward    bot__OnPlayerConnect(playerid);
#endif 


#if    defined    _ALS_OnIncomingConnection
    #undef    OnIncomingConnection
#else
    #define    _ALS_OnIncomingConnection
#endif
#define    OnIncomingConnection    bot__OnIncomingConnection
#if    defined    bot__OnIncomingConnection
forward    bot__OnIncomingConnection(playerid, ip_address[], port);
#endif 

#if    defined    _ALS_OnGameModeInit
    #undef    OnGameModeInit
#else
    #define    _ALS_OnGameModeInit
#endif
#define    OnGameModeInit    bot__OnGameModeInit
#if    defined    bot__OnGameModeInit
forward    bot__OnGameModeInit();
#endif 
